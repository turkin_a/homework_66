import React, { Component } from 'react';
import './CountryInfo.css';
import CountryData from "./CountryData/CountryData";
import Loader from "../UI/Loader/Loader";

class CountryInfo extends Component {
  shouldComponentUpdate(newProps) {
    return (newProps.isLoading !== this.props.isLoading);
  }

  render() {
    return (
      <div className="CountryInfo">
        {this.props.isLoading ?
          <Loader />
          : (this.props.country.name ?
            <CountryData
              country={this.props.country}
              borders={this.props.borders}
              click={this.props.click}
            /> : <div className="StartPage">Please select a country</div>)}
      </div>
    );
  }
}

export default CountryInfo;