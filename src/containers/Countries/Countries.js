import React, { Component } from 'react';
import axios from '../../axios-countries';
import './Countries.css';
import CountriesList from "../../components/CountriesList/CountriesList";
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";

class Countries extends Component {
  state = {
    countriesList: [],
    selectedCountry: {name: null},
    currentBorders: [],
    isListLoading: true,
    isLoading: false,
    onList: true
  };

  getBorder = border => {
    return axios.get(`alpha/${border}`).then(response => response.data.name);
  };

  selectCountry = (country, onList) => {
    if (country === this.state.selectedCountry.name) return null;
    this.setState({isLoading: true});

    axios.get(`name/${country}?fullText=true`).then(response => {
      this.setState({selectedCountry: response.data[0], onList});
      return Promise.all(response.data[0].borders.map(border => this.getBorder(border)));
    }).then(currentBorders => {
      this.setState({currentBorders, isLoading: false});
    }).catch(error => {
      console.log(error);
      this.setState({isLoading: false});
    });
  };

  componentDidMount() {
    axios.get('all?fields=name').then(response => {
      return Promise.all(response.data.map(country => {
          return country.name;
        })
      );
    }).then(countriesList => {
      this.setState({countriesList, isListLoading: false})
    }).catch(error => {
      console.log(error);
      this.setState({isListLoading: false})
    });
  }

  render() {
    return (
      <div className="Countries">
        <CountriesList
          isListLoading={this.state.isListLoading}
          countries={this.state.countriesList}
          selected={this.state.selectedCountry.name}
          click={this.selectCountry}
          onList={this.state.onList}
        />
        <CountryInfo
          country={this.state.selectedCountry}
          borders={this.state.currentBorders}
          isLoading={this.state.isLoading}
          click={this.selectCountry}
        />
      </div>
    );
  }
}

export default withErrorHandler(Countries, axios);